from openerp.osv import fields
from openerp.osv import osv
import openerp
from openerp.tools.translate import _
from openerp import SUPERUSER_ID


class res_users_agent(osv.osv):
	_inherit = "res.users"
	_columns = {
	'is_sales_agent':fields.boolean('Sales Agent'),
	}


class crm_lead_agent(osv.osv):
	_inherit = "crm.lead"
	_columns = {
	
	}


	def create_users(self, cr, uid, req_id, context=None):
		name = req_id.partner_name
		login = req_id.email_from
		res_users_obj = self.pool.get("res.users")
		values = {'name':name,'login':login, 'is_sales_agent':True}
		try:
			user_id = res_users_obj.create(cr, uid, values)
		except:
			raise openerp.exceptions.Warning(_('Unable to create User...'))
		return user_id

	def make_them_users(self, cr, uid, ids, context=None):
		obj = self.browse(cr, uid, ids[0])
		res_users_obj = self.pool.get("res.users")
		res_partner_obj = self.pool.get("res.partner")
		user_id = self.create_users(cr, uid, obj)
		user = res_users_obj.browse(cr, uid, user_id, context=context)	
		res_partner_obj.write(cr, uid, int(user.partner_id), {'customer':False, 'salesagent':True})

		gs = []
		groups_ids = self.pool.get('res.groups').search(cr,uid,[('name', '=', 'Agent')],context=context)
		gs.append(self.pool['res.groups'].browse(cr, SUPERUSER_ID, groups_ids[0], context=context).id)

		vals = {'groups_id': [(4, g) for g in gs]}
		res_users_obj.write(cr, uid, [user.id], vals, context)		
		return
