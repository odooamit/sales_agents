{
	'name':'Agents Portal',
	'version':'1.0',
	'author':'Jothimani R',
	'website':'www.jothimani.com',
	'summary':'Agents Portal',
	'description':"""This module is used for agents portal""",
	'depends':['crm', 'salesagent_commissions',],
	#'data':['security/partner_portal_security.xml',
	#		'security/ir.model.access.csv',
	#],
	'update_xml':['view/crm_lead_view.xml',],
	'installable':True
}
