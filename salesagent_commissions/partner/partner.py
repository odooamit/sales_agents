from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp
from openerp.addons.base_geolocalize.models.res_partner import geo_find, geo_query_address
import geopy
import geopy.distance

def find_coords(c, l):
    tmp_list = []
    (x,y) = l[0]
    for (a,b) in l[1:]:
            if (x-c[0])**2 + (y-c[1])**2 > (a-c[0])**2 + (b-c[1])**2:
                    (x,y) = (a,b)
                    tmp_list.append((x,y))					
    return tmp_list 

class res_partner(osv.osv):

    _inherit = "res.partner"

    def create(self, cr, uid, vals, context=None):
        us_id = super(res_partner, self).create(cr, uid, vals, context)
        self.assign_geo_localize(cr, uid, [us_id], context=context)
        return us_id
    
    def fill_products(self, cr, uid, ids, context=None):
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('standard_commission_product', '=', True)])
        if not product_ids:
            return True
        products = product_obj.browse(cr, uid, product_ids)
        for id in ids:
            for product in products:
                self.pool.get('partner.product_commission').create(
                    cr, uid, {'partner_id':id, 'name':product.id})
        return True

    def fill_categories(self, cr, uid, ids, context=None):
        category_obj = self.pool.get('product.category')
        category_ids = category_obj.search(
            cr, uid, [('standard_commission_category', '=', True)])
        if not category_ids:
            return True
        categories = category_obj.browse(cr, uid, category_ids)
        for id in ids:
            for category in categories:
                self.pool.get('partner.category_commission').create(cr, uid,
                    {'partner_id':id, 'name':category.id})
        return True
#   jothimani calculate partner id from user_id to view their customers		
    def _compute_partner_user_id(self, cr, uid, ids, name, args, context=None):
        res = {}			
        res_users_obj = self.pool.get('res.users')
        for obj in self.browse(cr, uid, ids, context=context):	
            if obj.salesagent_for_customer_id:
                partner_id = obj.salesagent_for_customer_id.id
                user_ids = res_users_obj.search(cr, uid, [('partner_id', '=', int(partner_id))], context=None)
                #raise openerp.exceptions.Warning(_(user_ids))  
                if user_ids:
                    res[obj.id] = int(user_ids[0])
                else:
                    res[obj.id] = partner_id
        return res
# jothimani end
		
    _columns = {
        'salesagent' : fields.boolean("Salesagent"),
        # ----- Relation with customers
        'customer_for_salesagent_ids' : fields.one2many('res.partner', 'salesagent_for_customer_id', 'Customers', readonly=True),
        # ----- Relation with salesagent
        'salesagent_for_customer_id': fields.many2one('res.partner', 'Salesagent'),
        # ----- General commission for salesagent
        'commission' : fields.float('Commission %'),
        'product_provvigioni_ids' : fields.one2many('partner.product_commission', 'partner_id', 'Commission for products'),
        'category_provvigioni_ids' : fields.one2many('partner.category_commission', 'partner_id', 'Commission for categories'),
        # -------- Filtering customers for current login user by jothimani
        'partner_user_id': fields.function(_compute_partner_user_id, type="integer", store=True),	
        'agent_latitude': fields.float('Geo Latitude'),
        'agent_longitude': fields.float('Geo Longitude'),		
    }
    
    def assign_geo_localize(self, cr, uid, ids, latitude=False, longitude=False, context=None):
        if latitude and longitude:
            self.write(cr, uid, ids, {
                'agent_latitude': latitude,
                'agent_longitude': longitude
            }, context=context)
            return True
        # Don't pass context to browse()! We need country name in english below
        for user in self.browse(cr, uid, ids):
            if user.agent_latitude and user.agent_longitude:
                continue
            if user.country_id:
                result = geo_find(geo_query_address(street=user.street,
                                                    zip=user.zip,
                                                    city=user.city,
                                                    state=user.state_id.name,
                                                    country=user.country_id.name))
                if result:
                    self.write(cr, uid, [user.id], {
                        'agent_latitude': result[0],
                        'agent_longitude': result[1]
                    }, context=context)
        return True     
    
    def action_assign_sale_agent(self, cr, uid, ids, context=None):
        self.assign_geo_localize(cr, uid, ids, context=context)
        user = self.browse(cr, uid, ids[0], context=context)
        partner_ids = []
        coordinate_list = []
        i = 1
        coordinate = (user.agent_latitude, user.agent_longitude)
        
        if coordinate:
            partner_ids = self.search(cr, uid,[('salesagent', '=', True)], context=context)
            agent_ids = self.browse(cr, uid, partner_ids)
            for agent_id in agent_ids:
                    coordinate_list.append((agent_id.agent_latitude, agent_id.agent_longitude))

	'''while coordinate_list[1:]:      
                coordinate_list = find_coords(coordinate, coordinate_list)
                i+=1'''
	'''dist=lambda s,d: (s[0]-d[0])**2+(s[1]-d[1])**2
	final = min(coordinate_list, key=partial(dist, coordinate))'''

        pts = [ geopy.Point(p[0],p[1]) for p in coordinate_list ]
        onept = geopy.Point(coordinate[0],coordinate[1])
        alldist = [ (p,geopy.distance.distance(p, onept).km) for p in pts ]
        nearest_point = min(alldist, key=lambda x: (x[1]))[0]
            
        for agent_id in agent_ids:
            if (agent_id.agent_latitude,agent_id.agent_longitude) == tuple(nearest_point[:2]):
                salesagent_for_customer_id = agent_id.id
            
        return self.write(cr, uid, [user.id], {'salesagent_for_customer_id': salesagent_for_customer_id}, context=context)

class partner_product_commission(osv.osv):

    _name = "partner.product_commission"
    _description = "Relation for Partner, products and commissions"

    _columns = {
        'name' : fields.many2one('product.product', 'Product'),
        'commission' : fields.float('Commission'),
        'partner_id' : fields.many2one('res.partner', 'Partner'),
        }

partner_product_commission()


class partner_category_commission(osv.osv):

    _name = "partner.category_commission"
    _description = "Relation for Partner, product categories and commissions"

    _columns = {
        'name' : fields.many2one('product.category', 'Category'),
        'commission' : fields.float('Commission'),
        'partner_id' : fields.many2one('res.partner', 'Partner'),
        }

partner_product_commission()
